
import './ResetPassword.css';
import React from 'react';

import ReactDOM from 'react-dom';
import {useHistory} from 'react-router-dom';

function ResetPassword()
{
  const history = useHistory();

  const scrollToTop = () =>{
    window.scrollTo({
      top: 0
      /* you can also use 'auto' behaviour
         in place of 'smooth' */
    });
  };

    const clickSubmit_Reset = ()=>
    {
      alert('Password has been changed');
      history.push('/login');
      scrollToTop();
    };

  return(
    <div class="reset-password-desktop">
      <div class="overlap-group">
        <div class="background">
          <div class="overlap-group">
            <div class="rectangle-1"></div>
            <div class="rectangle-8"></div>
          </div>
        </div>
        <div class="title">
          <div class="epotcom milonga-normal-black-36px">epot.com</div>
          <img
            class="reset-password"
            src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/60802b201c8a2ea23ed1e57b/img/reset-password@2x.svg"
          />
          <img
            class="rectangle-2"
            src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/607ea9af79ee560c8a4a7118/img/rectangle-2@1x.png"
          />
        </div>
        <div class="reset-pass">
          <h1 class="welcome publicsans-normal-black-64px">Welcome back,</h1>
          <div class="text-1 publicsans-normal-black-30px">Please enter your new password!</div>
          <div class="edit-text-password">
            <input
              class="text-i6157 publicsans-thin-black-24px smart-layers-pointers"
              name="text"
              placeholder="Password"
              type="password"
              required
            />
          </div>
          <div class="edit-text-confirm-password">
            <input
              class="text-i6157 publicsans-thin-black-24px smart-layers-pointers"
              name="text"
              placeholder="Confirm password"
              type="password"
              required
            />
          </div>
          <div class="change-password-button smart-layers-pointers">
            <div class="text-i6157543162 valign-text-middle publicsans-medium-cod-gray-30px" onClick={clickSubmit_Reset}>Change Password</div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default ResetPassword;
