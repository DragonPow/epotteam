
import './Login.css';
import React from 'react';

import ReactDOM from 'react-dom';
import {useHistory} from 'react-router-dom';

function Login() {

  const history = useHistory();

  const scrollToTop = () =>{
    window.scrollTo({
      top: 0
      /* you can also use 'auto' behaviour
         in place of 'smooth' */
    });
  };

  const clickForgot = ()=>
  {
      history.push('/forgot');
      scrollToTop();
  };

  const clickRegister = ()=>
  {
      history.push('/register');
      scrollToTop();
  };

  const clickLoginWithGG = ()=>{
    alert('Login with GG');
    clickSubmit_Login();
  };

  const clickSubmit_Login = ()=>{
    history.push('/');
    scrollToTop();
  };

  return(
    <div class="login-desktop">
  <div class="overlap-group">
    <div class="background">
      <div class="overlap-group">
        <div class="rectangle-9"></div>
        <div class="rectangle-1"></div>
        <div class="rectangle-8"></div>
      </div>
    </div>
    <div class="group-2">
      <div class="or publicsans-medium-asphalt-30px">or</div>
      <div class="flex-col">
        <h1 class="title publicsans-medium-licorice-64px">Log In</h1>
        <div class="input-with-icon">
          <img
            class="icon"
            src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/608000cd737402200c04e882/img/icon-1@2x.png"
          />
          <input
            class="text-i6152 publicsans-thin-black-30px smart-layers-pointers"
            name="text"
            placeholder="Username"
            type="text"
            required
          />
        </div>
        <div class="password-input">
          <img
            class="icon"
            src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/608000cd737402200c04e882/img/icon@2x.png"
          />
          <input
            class="text-i6152 publicsans-thin-black-30px smart-layers-pointers"
            name="text"
            placeholder="Password"
            type="password"
            required
          />
        </div>
        <div class="login-button smart-layers-pointers">
          <div class="text-i615 valign-text-middle publicsans-medium-cod-gray-30px" onClick={clickSubmit_Login}>Login</div>
        </div>
        <div class="flex-row">
          <div class="forgot-password" onClick={clickForgot}>Forgot password?</div>
          <div class="sign-up smart-layers-pointers" onClick={clickRegister}>Sign up!</div>
        </div>
        <div class="login-gg-button smart-layers-pointers">
          <div class="text-i615 valign-text-middle publicsans-medium-cod-gray-30px" onClick={clickLoginWithGG}>Continue with Google</div>
        </div>
      </div>
    </div>
    <div class="group-1">
      <img
        class="welcome-to"
        src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/607ea7448905095dce87288a/img/welcome-to-1@2x.svg"
      />
      <div class="overlap-group2">
        <img
          class="vector-1"
          src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/60800d130f89b2cce0ddf1d2/img/vector-2@1x.svg"
        />
        <img
          class="vector"
          src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/60800d130f89b2cce0ddf1d2/img/vector-3@1x.svg"
        />
        <img
          class="epot"
          src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/60800d130f89b2cce0ddf1d2/img/epot-1@2x.svg"
        />
      </div>
    </div>
  </div>
</div>
  );
}
export default Login;
