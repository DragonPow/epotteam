import './Register.css'

import React from 'react';

import {useHistory} from 'react-router-dom';

function Register() {
  const history = useHistory();

  const scrollToTop = () =>{
    window.scrollTo({
      top: 0
      /* you can also use 'auto' behaviour
         in place of 'smooth' */
    });
  };

  const clickLogin = ()=>
  {
      history.push('/login');
      scrollToTop();
  };

  const clickSubmit_Regist = ()=>{
    alert('Registion complete');
    history.push('/login');
    scrollToTop();
  };

  return (
    <div class="register-desktop">
      <div class="overlap-group">
        <div class="background">
          <div class="overlap-group1">
            <div class="rectangle-9"></div>
            <div class="rectangle-1"></div>
            <div class="rectangle-8"></div>
          </div>
        </div>
        <div class="sign-up">
          <h1 class="title publicsans-medium-licorice-64px">Sign Up</h1>
          <div class="input">
            <input
              class="text-i615 publicsans-thin-black-24px smart-layers-pointers"
              name="text"
              placeholder="Email address"
              type="text"
              required
            />
          </div>
          <div class="input">
            <input
              class="text-i615 publicsans-thin-black-24px smart-layers-pointers"
              name="text"
              placeholder="Username"
              type="text"
              required
            />
          </div>
          <div class="input">
            <input
              class="text-i615 publicsans-thin-black-24px smart-layers-pointers"
              name="text"
              placeholder="Password"
              type="password"
              required
            />
          </div>
          <div class="sign-up-button smart-layers-pointers">
            <div class="text-i6154743162 valign-text-middle publicsans-medium-cod-gray-30px" onClick={clickSubmit_Regist}>Sign up</div>
          </div>
          <div class="sign-in-frame">
            <div class="already-a-account publicsans-thin-black-24px">Already a account?</div>
            <div class="sign-in smart-layers-pointers" onClick={clickLogin}>Sign in</div>
          </div>
        </div>
        <div class="group-1">
          <div class="epotcom milonga-normal-black-36px">epot.com</div>
          <img
            class="welcome-to"
            src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/607e9315e76b518dc655c0e0/img/welcome-to@2x.svg"
          />
          <div class="overlap-group2">
            <img
              class="vector"
              src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/607eab698b2f296e23040dd9/img/vector-2@1x.svg"
            />
            <img
              class="vector-1"
              src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/607e9315e76b518dc655c0e0/img/vector-1@1x.svg"
            />
            <img
              class="epot"
              src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/607ea7448905095dce87288a/img/epot-1@2x.svg"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;
