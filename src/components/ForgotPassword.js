
import './ForgotPassword.css';
import React from 'react';

import ReactDOM from 'react-dom';
import {useHistory} from 'react-router-dom';

function ForgotPassword()
{
  const history = useHistory();

  const scrollToTop = () =>{
    window.scrollTo({
      top: 0
      /* you can also use 'auto' behaviour
         in place of 'smooth' */
    });
  };

  const clickLogin = ()=>
  {
      history.push('/login');
      scrollToTop();
  };

  const clickSendcode = ()=>
  {
      alert('Code has been seen!');
  };

  const clickSubmit_Forgot = ()=>
  {
    alert('Vertification is complete');
    history.push('/resetpass');
    scrollToTop();
  };


  return(
    <div class="forgot-password-desktop">
      <div class="overlap-group">
        <div class="background">
          <div class="overlap-group">
            <div class="rectangle-9"></div>
            <div class="rectangle-1"></div>
            <div class="rectangle-8"></div>
          </div>
        </div>
        <div class="title">
          <div class="epotcom milonga-normal-black-36px">epot.com</div>
          <img
            class="forgot-password"
            src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/608018ff8766dda016020594/img/forgot-password-@1x.png"
          />
          <img
            class="rectangle-2"
            src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/607ea9af79ee560c8a4a7118/img/rectangle-2@1x.png"
          />
        </div>
        <div class="dont-worry">
          <h1 class="dont-worry-1 publicsans-light-black-64px">Don’t worry!</h1>
          <div class="text-1 publicsans-thin-black-30px">
            The verification will be send in mailbox.<br />Please&nbsp;&nbsp;check it.
          </div>
          <div class="email-input">
            <input
              class="text-i6159 publicsans-thin-black-24px smart-layers-pointers"
              name="text"
              placeholder="Email address"
              type="text"
              required
            />
          </div>
          <div class="code-input">
            <div class="overlap-group3">
              <input
                class="text-i6159 publicsans-thin-black-24px smart-layers-pointers"
                name="text"
                placeholder="Vertification code"
                type="text"
                required
              />
            </div>
          </div>
          <div class="flex-row">
            <div class="flex-col">
              <div class="button-send smart-layers-pointers">
                <div class="overlap-group1">
                  <img
                    class="rectangle-5"
                    src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/608018ff8766dda016020594/img/rectangle-5@2x.svg"
                  />
                  <div class="text-i6159643470 valign-text-middle publicsans-medium-cod-gray-30px" onClick={clickSendcode}>Send</div>
                </div>
              </div>
              <div class="back-to-login publicsans-thin-vista-white-24px smart-layers-pointers" onClick={clickLogin}>Back to login</div>
            </div>
            <div class="button-next smart-layers-pointers">
              <div class="overlap-group2">
                <img
                  class="rectangle-5-1"
                  src="https://anima-uploads.s3.amazonaws.com/projects/607e92f15551a9ba11c316ad/releases/608018ff8766dda016020594/img/rectangle-5-1@2x.svg"
                />
                <div class="text-i6159743470 valign-text-middle publicsans-medium-cod-gray-30px" onClick={clickSubmit_Forgot}>Next</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default ForgotPassword;
